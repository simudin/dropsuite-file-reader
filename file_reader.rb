require "yaml"
require 'fileutils'

# Print out in console to get argument of path and sanitize
# for parsing it
print "Give me the directory path or configuration file(yaml):"
path = gets.chomp()
path = path.delete("'").delete('"').delete(" ")

# Check if the path is a yaml file
# the yaml content should follow the requested format
if(path.include?(".yml"))
  path = YAML.load_file(path)
  path = path['directory_path']
end

# Scan the path to get  all files in directory/subdirectory
path = path.to_str + "/**/*"
files = Dir.glob(path).reject {|f| File.directory?(f)}

# Prepare for intilizing variable that will be used to
# present the last output
previous_content = {paths: [], filenames: []}

# Start iterating the files to get matches files
files.each do |file1|
  next if previous_content[:paths].include?(file1) # go to next file if the path is already present before

  current_content = {paths: [file1], filenames: [File.basename(file1)]} # store the variables for comparing in the last output

  # Each file will be reiterate to the first file to find matching content
  files.each do |file2|
    next if file1 == file2 # go to next iteration if the files have same path(it means the same file)
    next if previous_content[:paths].include?(file2) # go to next iteration if the file is already compared before

    # Compare file using `compare_file` method from
    # the fileutils library: https://apidock.com/ruby/FileUtils/compare_file
    # it will return true if the contents of both file are identical
    if FileUtils.compare_file(file1, file2)
      current_content[:filenames].push(File.basename(file2)) # store the variable to be used in the next iteration
      current_content[:paths].push(file2)
    end
  end

  # Replace the previous content of iterations with the new content if the current content have more identical files
  previous_content = current_content if previous_content[:filenames].count < current_content[:filenames].uniq.count
end

# Present the output
puts "#{File.read(previous_content[:paths][0])} #{previous_content[:filenames].uniq.count}"
