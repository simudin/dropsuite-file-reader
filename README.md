# Dropsuite File Reader

Get the count of identical files in directory

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

What things you need to install:

```
ruby 2.6.3

```

### Installing

1. Clone the repo or just copy the `file_reader.rb` script
2. Open terminal and go to the folder where the script exist.
3. Make sure you have ruby installed and then just run:

```
ruby file_reader.rb
```

4. And then the script will ask you to write the path to directory or the path to the configuration(yaml) file
```
Give me the directory path or configuration file(yaml):
```
5. You can just drag and drop the directory or yaml file to the terminal or you can type the path.
6. Make sure you type the path correctly and also for yaml file you have to follow the example of yaml file that required by the script. See the right yaml file exmaple here[https://bitbucket.org/simudin/dropsuite-file-reader/src/492a537e90b2abfb7e1c86ba25cbd1456bc045b2/yaml_example.yml?at=master]
```
Give me the directory path or configuration file(yaml):/home/robotahu/Downloads/DropsuiteTest/DropsuiteTest
or
Give me the directory path or configuration file(yaml):/home/robotahu/Downloads/DropsuiteTest/example_yaml.yml
```
7. After you insert the path, just Enter
8. The output will be like this:
```
abcd 4
```
